Hello and welcome to our simple, easy to follow guide on running our project. This was made by Ashraf Abi Said, Ayman El Hajj, Jihad Eddine Al Khurfan, Rayyan Nasr, and Sebastian El Khoury.

Our program is a demo of the true program, so some parts will not be used in the actual live version of the code. These will be highlighted as they are reached.

We used WAMPP server to simulate an apache server, since we require a mock version of the database. These will be provided by the company, but for the sake of testing we made our own.
1.	Install the WAMPP server
2.	Open phpMyAdmin
3.	Login with �root� as username and nothing as password
4.	Under SQL, create a new table called mock_input_db
5.	For this table, choose the �Import� button and choose db.sql 
You now have the database that will be read by the code.

In order to run the project, you must add all the files to a project in the WAMPP server as well. We put them in C:\wamp64\www\Projects\ProjectName

Next, we need to open the website. To do so, log in to your favorite browser and write (or copy paste) the following:
http://localhost/Projects/ProjectName/login.html

For the username and password, you have the choice of any of the following:
ayman	potato
ashraf	tomato
rayan	raddish
sebas	eggs
jihad	carrot
Anything else will give an error.


