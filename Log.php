<?php
require("Rest.inc.php");
require("Database.inc.php");
require("API_request.inc.php");

// Start Session
session_start();
if (!isset($_SESSION['logged_in'])) {
    $_SESSION['login_time'] = 0;
    header('Location: login.html');
}


// Some settings for the API:
// Output Buffer start
ob_start();
// Maximum Memory Limit
// In case the output was too large, the next 2 options ensure max output size and exec time
ini_set('memory_limit', 0);
ini_set('max_execution_time', 0);
date_default_timezone_set('Asia/Beirut');
error_reporting(1);

// Initiate Library
$api = new api();
$api->processapi();
?>
