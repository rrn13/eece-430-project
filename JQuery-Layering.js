/////////////////////////////////////////// JQuery Functions //////////////////////////////////////////


var username = null;
var password = null;
const representation = ['table', 'raw', 'RegionAreaData', 'AnnotationChartData', 'operatorsRevenueGraph', 'countryDropdown',
    'operatorDropdown', 'servicesDropdown', 'servicesRevenueGraph', 'pieChart', 'countriesButtonGroup', 'annotOptsData', 'annotRevsData',
    'optsChart', 'optsChartService', 'optIChart', 'optOChart', 'optFChart'];
const requests = ['getCountry', 'getOperatorsRevenue', 'getOperators', 'getServices', 'getOptins', 'getOptouts', 'getOptfails', 'getOpts', 'getChargingRate', 'getServicesRevenue', 'getOptsOperators'];
const countriesDataContainer = '#countries-data-container';
const revenuesContainer = '#revenues-container';
const operatorsContainer = '#operators-container';
const servicesContainter = '#services-container';
const optsContainer = '#opts-container';
const countriesTableView = '#countries-table-view';
const countriesMapView = '#countries-map-view';
const revenuesTableView = '#revenues-table-view';
const revenuesGraphView = '#revenues-graph-view';
const optsGraphView = '#opts-graph-view';
const optsTableView = '#opts-table-view';
const chartContainer = 'chart-container';
const selectCountry = 'select-country';
const selectOperator = 'select-operator';
const selectService = 'select-service';
const clickCoordinatesTable = 'click-coordinates-table';
const exportWrapper = '#export-wrapper';
const homeNav = "#home-nav";
const operatorsNav = "#operators-nav";
const servicesNav = "#services-nav";
const optsNav = "#opts-nav";
const annotationNav = "#annotation-nav";
const revenuesNav = "#revenues-nav";
const aboutNav = "#about-nav";
const optsButton = '#opts-button';
const optInsButton = '#opt-ins-button';
const optOutsButton = '#opt-outs-button';
const optFailsButton = '#opt-fails-button';
const alertContainer = '#alert-container';
const countriesGroup = '#countries-group';
const annotChart = 'annot-chart';
const optsStats = '#opts-stats';
const revsStats = '#revs-stats';
const optsSelector = '#opts-selector';

var RegionAreaData = [];
var OptsAnnotationChartData = [];
var RevsAnnotationChartData = [];
var pieChartOp;
var chart2;

// This function executes when the document is ready, you can add different types of functions with different purposes

$(document).ready(function () {

    username = Cookies.get('username');
    password = Cookies.get('password');
    if (!username || !password) {
        window.location.href = 'Login.html'; // Make sure user has a session, if not only show login page
        return false;
    } else {
        username = Cookies.get('username');
        $.post('Log.php', {
            request: 'login',
            username: username,
            password: password,
        }, function (response) {
            Cookies.set('username', username);
            Cookies.set('password', password);
        }).fail(function () {
            alert('Failed to retrieve session!');
            window.location.href = 'Login.html'; // Make sure user has a session, if not only show login page
        })
    }

    $.getRegionAreaData();
    // Activate pressed nav elements and deactivate unpressed on click
    // This button presents the table of all countries on home page when pressed

    $(countriesTableView).on('click', function () {
        $(countriesDataContainer).empty();
        $('button.nav-link').removeClass('active');
        $(this).addClass('active');
        $.sendRequest(requests[0], countriesDataContainer, representation[0]);
        $(alertContainer).prepend("<div class=\"alert alert-info\" role=\"alert\">\n" +
            "  All countries with operators" +
            "</div>")
    });

    // This button presents the Area Map of countries when pressed instead of a table

    $(countriesMapView).on('click', function () {
        $(countriesDataContainer).empty();
        $('button.nav-link').removeClass('active');
        $(this).addClass('active');
        google.charts.load('current', {
            'packages': ['geochart'],
            // Note: you will need to get a mapsApiKey for your project.
            // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
            'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
        });
        google.charts.setOnLoadCallback(drawRegionsMap);
        window.innerWidth;
        window.innerHeight;

        function drawRegionsMap() {
            chartDrawerHelper("regionMap")
        }
    });

    // Default action is clicked

    $(countriesMapView).click();

    // This function loads the table of all revenues when pressed

    $(revenuesTableView).on('click', function () {
        $(alertContainer).empty();
        $(revenuesContainer).empty();
        $(exportWrapper).empty();
        $('button.nav-link').removeClass('active');
        $(this).addClass('active');
        $.sendRequest(requests[1], revenuesContainer, representation[0]);
        $(alertContainer).prepend("<div class=\"alert alert-info\" role=\"alert\">\n" +
            "  All operators revenues in all countries" +
            "</div>")
    });

    // This loads the page with revenues graph instead of table

    $(revenuesGraphView).on('click', function () {
        $(alertContainer).empty();
        $(exportWrapper).empty();
        $(exportWrapper).append('<button type="button" id="exportChart" class="btn btn-info float-right">Export Chart</button>');
        $(revenuesContainer).empty();
        $(revenuesContainer).append('<div id="chart-container" style="height: 500px; width: 100%;"></div><br>' +
            '<div id="click-coordinates-table" style="width: 100%; height: 400px">' +
            '<div style="height: 200px"></div></div>');
        $('button.nav-link').removeClass('active');
        $(this).addClass('active');
        $.sendRequest(requests[1], chartContainer, representation[4]);
        $(alertContainer).prepend("<div class=\"alert alert-info\" role=\"alert\">\n" +
            "  All operators revenues in all countries" +
            "</div>");
        $("#myInput").hide();
        $("table").hide();
    });

    // Default action is clicked

    $(revenuesGraphView).click();

    // This function loads the table of all opts when pressed

    $(optsTableView).on('click', function () {
        $(alertContainer).empty();
        $(optsContainer).empty();
        $(exportWrapper).empty();
        $('button.nav-link').removeClass('active');
        $(this).addClass('active');
        $(optsSelector).show();
        var request;
        if ($(optsButton).hasClass('active')) {
            request = requests[10];
        } else if ($(optInsButton).hasClass('active')) {
            request = requests[4];
        } else if ($(optOutsButton).hasClass('active')) {
            request = requests[5];
        } else if ($(optFailsButton).hasClass('active')) {
            request = requests[6];
        }
        $.sendRequest(request, optsContainer, representation[0]);
        $(alertContainer).prepend("<div class=\"alert alert-info\" role=\"alert\">\n" +
            "  All " + request + " information for all countries" +
            "</div>")
    });

    // This loads the page with revenues graph instead of table

    $(optsGraphView).on('click', function () {
        $(alertContainer).empty();
        $(exportWrapper).empty();
        $(exportWrapper).append('<button type="button" id="exportChart" class="btn btn-info float-right">Export Chart</button>');
        $(optsContainer).empty();
        $(optsContainer).append('<div id="chart-container" style="height: 500px; width: 100%;"></div><br>' +
            '<div id="click-coordinates-table" style="width: 100%;">' +
            '<div style="height: 200px"></div></div>');
        $('button.nav-link').removeClass('active');
        $(this).addClass('active');
        $(optsSelector).hide();
        $.sendRequest(requests[10], chartContainer, representation[13]);
        $("#myInput").hide();
        $("table").hide();
    });

    // Default action is clicked
    $(optsButton).click();
    $(optsTableView).click();
    // Colors for the bars in the bar graph
    CanvasJS.addColorSet("revenuesColorSet",
        ['#007bff',
            '#6f42c1',
            '#e83e8c',
            '#28a745',
            '#20c997',
            '#17a2b8',]);

    CanvasJS.addColorSet("optsColorSet",
        ['#28a745',
            '#007bff',
            '#dc3545',]);
    CanvasJS.addColorSet("pieColorSet",
        ['#007bff',
            '#28a745',
            '#e83e8c',
            '#17a2b8',
            '#6f42c1',
            '#ffc107',
            '#dc3545',]);
});

// These functions load the global array for AnnotationChart and RegionChart

$.getRegionAreaData = function () {
    $.sendRequest(requests[2], null, representation[2]);
};
// Data preparation for Revenue, Opt stats
$.prepareAnnotationData = function (country = null, type) {
    RevsAnnotationChartData = [];
    OptsAnnotationChartData = [];
    RevsAnnotationChartData[0] = [{label: 'Month', type: 'string'}, {label: 'Revenue', type: 'number'}];
    OptsAnnotationChartData[0] = [{label: 'Month', type: 'string'}, {label: 'OptIn', type: 'number'}, {
        label: 'OptOut',
        type: 'number'
    }];
    var req, rep;
    if (type === 'revs') {
        req = 1;
        rep = 12;
    } else if (type === 'opts') {
        req = 10;
        rep = 11;
    }
    var data = {
        request: requests[req],
        username: username,
        password: password,
    };

    if (country) {
        data.country = country;
    }

    var endDate = convertDate(new Date());
    var dataPoints = parseInt(endDate.substr(5, 2));
    for (var i = 1; i <= dataPoints; i++) {
        var sDate = endDate.substr(0, 4) + '-0' + i.toString() + '-01';
        var eDate = endDate.substr(0, 4) + '-0' + i.toString() + '-14';
        // I exceptionally sent datapoints instead of container
        // I used async so that no row gets pushed before the other
        data.start_date = sDate;
        data.end_date = eDate;
        $.ajax({
            async: false,
            type: "POST",
            url: "Log.php",
            data: data,
            dataType: "json",
            success: function (data) {
                $.getData(data, dataPoints, representation[rep])
            }
        });
        sDate = endDate.substr(0, 4) + '-0' + i.toString() + '-15';
        eDate = endDate.substr(0, 4) + '-0' + (i + 1).toString() + '-01';
        // I exceptionally sent datapoints instead of container
        // I used async so that no row gets pushed before the other
        data.start_date = sDate;
        data.end_date = eDate;
        $.ajax({
            async: false,
            type: "POST",
            url: "Log.php",
            data: data,
            dataType: "json",
            success: function (data) {
                $.getData(data, dataPoints, representation[rep])
            }
        });
    }
};

// These functions process the callback function data of the previous requests

$.loadRegionAreaData = function (data) {
    var arr = [];
    data = $.parseJSON(JSON.stringify(data));
    $.each(data, function (i, v) {
        if (v.Country === ('UAE' || 'United Arab Emirates'))
            v.Country = 'United Arab Emirates';
        arr[v.Country] = (arr[v.Country] || 0) + 1;
    });
    console.log(arr);
    RegionAreaData[0] = ['Country', 'Number of Operators'];
    var j = 1;
    for (i in arr) {
        RegionAreaData[j] = [i, arr[i]];
        j++;
    }
    console.log(RegionAreaData);
};

/*
    @title Helper function to draw Google API charts

    @param typeOfChart Depending of type of chart passed, a different render occurs on screen
 */
function chartDrawerHelper(typeOfChart, param) {

    $('#' + annotChart).empty();

    switch (typeOfChart) {
        case 'areaChart':
            switch (param) {
                case 'revs':
                    var chartData1 = google.visualization.arrayToDataTable(RevsAnnotationChartData);

                    var max = chartData1.getColumnRange(1).max;
                    var min = chartData1.getColumnRange(1).min;

                    var chartOptions1 = {
                        title: 'Revenue Stats ($)',
                        hAxis: {title: 'Month', titleTextStyle: {color: '#333'}},
                        vAxis: {minValue: 0},
                        backgroundColor: 'transparent',
                    };

                    var newChart1 = new google.visualization.AreaChart(document.getElementById(annotChart));
                    newChart1.draw(chartData1, chartOptions1);
                    $(alertContainer).empty();
                    $(alertContainer).prepend("<div class=\"alert alert-success h-100\" role=\"alert\">\n" +
                        " Max revenue: " + max + " | Min revenue : " + min + "</div>");
                    break;
                case 'opts':
                    var chartData2 = google.visualization.arrayToDataTable(OptsAnnotationChartData);
                    var maxOptins = chartData2.getColumnRange(1).max;
                    var maxOptouts = chartData2.getColumnRange(2).max;
                    var minOptins = chartData2.getColumnRange(1).min;
                    var minOptouts = chartData2.getColumnRange(2).min;
                    var chartOptions2 = {
                        title: 'Opt-Ins and Opt-Outs Stats',
                        hAxis: {title: 'Month', titleTextStyle: {color: '#333'}},
                        vAxis: {minValue: 0},
                        backgroundColor: 'transparent',
                    };
                    var newChart2 = new google.visualization.AreaChart(document.getElementById(annotChart));
                    newChart2.draw(chartData2, chartOptions2);
                    $(alertContainer).empty();
                    $(alertContainer).prepend("<div class=\"alert alert-primary h-100\" role=\"alert\">\n" +
                        " Max Optins: " + maxOptins + " | Min Optins : " + minOptins + "</div>");
                    $(alertContainer).append("<div class=\"alert alert-danger h-100\" role=\"alert\">\n" +
                        " Max Optouts: " + maxOptouts + " | Min Optouts : " + minOptouts + "</div>");
                    break;
            }
            break;

        case 'regionMap':
            var data = google.visualization.arrayToDataTable(RegionAreaData);

            var options = {
                region: '145', colorAxis: {colors: ['#28a745', '#007bff', '#6610f2']},
                backgroundColor: '#f8f9fa',
                datalessRegionColor: '#868e96',
                defaultColor: '#e9ecef',
            };

            var chart = new google.visualization.GeoChart(document.getElementById(countriesDataContainer.substr(1, countriesDataContainer.length)));

            chart.draw(data, options);
            google.visualization.events.addListener(chart, 'select', myClickHandler);
            break;
    }

    /*
      @title onCLick Handler function that displays details for Google Region Map
   */
    function myClickHandler() {
        var selection = chart.getSelection();
        var message = '';
        for (var i = 0; i < selection.length; i++) {
            var item = selection[i];
            if (item.row && item.column) {
                message += '{row:' + item.row + ',column:' + item.column + '}';
                alert(message);
            } else if (item.row) {

                message += 'Country :' + RegionAreaData[item.row + 1][0] + '\nOperators :' + RegionAreaData[item.row + 1][1];
                Cookies.set('Country', RegionAreaData[item.row + 1][0]);
                window.location.href = 'Operators.html';

            } else if (item.column) {
                message += '{column:' + item.column + '}';
                alert(message);
            }
        }
        if (message === '') {
            message = 'nothing';
            alert(message);
        }

    }
}

/*
    @title Function to filter table results
    @param inputID ID of region to be filtered in HTML
    @param tableID ID of table to be filtered in HTML
 */
$.filterTable = function (inputID, tableID) {
    $("#" + inputID).on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#" + tableID + " tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
};

// This function is a callback function that analyses post request returned data

$.getData = function (data, container = null, representation) {
    if (data === '{}')
        console.log("no value returned!");
    else {
        switch (representation) {  // cases to see which representation(Graph, Table etc...) we need to show
            case 'table':
                $.representTable(container, data);
                break;
            case 'raw':
                $.representRaw(container, data);
                break;
            case 'optIChart':
            case 'optOChart':
            case 'optFChart':
            case 'servicesRevenueGraph':
            case 'operatorsRevenueGraph':
                $.representGraph(container, data, representation);
                break;
            case 'countryDropdown':
                $.representCountryDropdown(container, data);
                break;
            case 'operatorDropdown':
                $.representOperatorDropdown(container, data);
                break;
            case 'servicesDropdown':
                $.representServicesDropdown(container, data);
                break;
            case 'RegionAreaData':
                $.loadRegionAreaData(data);
                break;
            case 'pieChart':
                $.representPieChart(container, data);
                break;
            case 'countriesButtonGroup':
                $.representCountryButtonGroup(container, data);
                break;
            case 'annotOptsData':
                $.sumAndPush(data, OptsAnnotationChartData, container);
                break;
            case 'annotRevsData':
                $.sumAndPush(data, RevsAnnotationChartData, container);
                break;
            case 'optsChartService':
            case 'optsChart':
                $.representOptsChart(container, data, representation);
                break;
            default:
                $.representRaw(container, data);
                break;
        }
    }
};
// Get total opt ins and opt outs to show in stats
$.sumAndPush = function (data, target, datapoints) {
    if (target === OptsAnnotationChartData) {
        var totalOptins = 0;
        var totalOptouts = 0;
        data = $.parseJSON(JSON.stringify(data));
        $.each(data, function (i, v) {
            totalOptins = (totalOptins || 0) + parseInt(v['Total Optins']);
            totalOptouts = (totalOptouts || 0) + parseInt(v['Total Optouts']);
        });
        var point = '';
        if (OptsAnnotationChartData.length % 2 === 0)
            point = '0' + (OptsAnnotationChartData.length / 2).toString();
        OptsAnnotationChartData.push([point, totalOptins, totalOptouts]);

    } else if (target === RevsAnnotationChartData) {
        var totalRevenue = 0;
        data = $.parseJSON(JSON.stringify(data));
        $.each(data, function (i, v) {
            totalRevenue = (totalRevenue || 0) + parseFloat(v['Revenue']);
        });
        var point = '';
        if (RevsAnnotationChartData.length % 2 === 0)
            point = '0' + (RevsAnnotationChartData.length / 2).toString();
        RevsAnnotationChartData.push([point, totalRevenue]);

    }
    if (RevsAnnotationChartData.length === datapoints) {
        console.log(RevsAnnotationChartData.length, OptsAnnotationChartData.length);
        console.log(RevsAnnotationChartData, OptsAnnotationChartData);
        google.charts.setOnLoadCallback(function () {
            chartDrawerHelper("areaChart", "revs");

        });
    } else if (OptsAnnotationChartData.length === datapoints) {
        console.log(RevsAnnotationChartData.length, OptsAnnotationChartData.length);
        console.log(RevsAnnotationChartData, OptsAnnotationChartData);
        google.charts.setOnLoadCallback(function () {
            chartDrawerHelper("areaChart", "opts");

        });
    }
};

// This function sends requests

$.sendRequest = function (request, container = null, representation = null, startDate = null, endDate = null, country = null, operator = null, service = null) {
    var params = {
        request: request,
        username: username,
        password: password,
    };
    if (startDate) params.start_date = startDate;
    if (endDate) params.end_date = endDate;
    if (country) params.country = country;
    if (operator) params.operator = operator;
    if (service) params.operator = service;
    $.post('Log.php', params, function (data) {
        if (!jQuery.isEmptyObject(data)) {
            console.log(request);
            $.getData(data, container, representation);
        }
    }, 'json').fail(request + 'failed.');

};


// This function populates the table with the received data and appends to the given container

$.representTable = function (container, data) {
    var table = $.makeTable(eval(data));
    $(container).empty();
    $(container).append('<div class="d-flex flex-row">' +
        '<input id="input-to-filter" type="text" class="form-control" placeholder="Search this table"  style="width: 65%"> <hr/>' +
        '<div class="d-flex flex-row justify-content-around">' +
        '<button id="json" class="btn btn-primary" style="margin-right: 5px; margin-left: 5px"><i class="fa fa-file-text-o" aria-hidden="true"></i> TO JSON</button>' +
        '<button id="csv" class="btn btn-info" style="margin-right: 5px"><i class="fa fa-file-excel-o" aria-hidden="true"></i> TO CSV</button>' +
        '<button id="pdf" class="btn btn-danger" style="margin-right: 5px"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> TO PDF</button></div>\n</div><br/>');
    $(container).append($(table));
    $.filterTable('input-to-filter', $(table).attr('id'));
    $('#json').on('click', function () {
        $(table).tableHTMLExport({type: 'json', filename: 'Report.json'});
    });
    $('#csv').on('click', function () {
        $(table).tableHTMLExport({type: 'csv', filename: 'Report.csv'});
    });
    $('#pdf').on('click', function () {
        $(table).tableHTMLExport({type: 'pdf', filename: 'Report.pdf'});
    });
};

// This function transforms a json format array into a table dynamically

$.makeTable = function (data) {
    var table = $('<table id="dataTable" class=\"table table-responsive-md table-striped\">');
    tblHeader = '<thead class="bg-secondary text-light"><tr>';
    for (var k in data[0]) {
        tblHeader += "<th >" + k + "</th>";
    }
    tblHeader += "</tr></thead>";
    $(tblHeader).appendTo(table);
    var TableRow = '';
    $.each(data, function (index, value) {
        TableRow += "<tr>";
        $.each(value, function (key, val) {
            TableRow += "<td>" + val + "</td>";
        });
        TableRow += "</tr>";
    });
    var tblBody = "<tbody>" + TableRow + "</tbody>";
    $(table).append(tblBody);
    return $(table);
};

// This function appends raw data in json format to the container

$.representRaw = function (container, data) {
    var raw = JSON.stringify(data);
    console.log(raw);
    $(container).empty();
    $('<p> ' + raw + '</p>').appendTo(container);
};
// This function appends the a graph to the container depending on represenation
$.representGraph = function (container, data, representation) {
    var d = [];
    if (representation === 'operatorsRevenueGraph') {
        for (var i = 0; i < data.length; i++) {
            d.push({
                label: data[i].Operator,
                y: parseFloat(data[i].Revenue)
            });
        }
        buildChart(container, d, "column", "Revenues", "Operators", "Revenues");
    } else if (representation === 'servicesRevenueGraph') {
        for (var j = 0; j < data.length; j++) {
            d.push({
                label: data[j].Service,
                y: parseFloat(data[j].Revenue)
            });
        }
        buildChart(container, d, "column", "Revenues", "Services", "Revenues");
    } else if (representation === 'optIChart') {
        for (var j = 0; j < data.length; j++) {
            d.push({
                label: data[j].Operator,
                y: parseFloat(data[j]["Total Optins"])
            });
        }
        buildChart(container, d, "column", "Opt Ins", "Operator", "Opt ins");
    } else if (representation === 'optOChart') {
        for (var j = 0; j < data.length; j++) {
            d.push({
                label: data[j].Operator,
                y: parseFloat(data[j]["Total Optouts"])
            });
        }
        buildChart(container, d, "column", "Opt Outs", "Operator", "Opt outs");
    } else if (representation === 'optFChart') {
        for (var j = 0; j < data.length; j++) {
            d.push({
                label: data[j].Operator,
                y: parseFloat(data[j]["Total Optfails"])
            });
        }
        buildChart(container, d, "column", "Opt Fails", "Operator", "Opt fails");
    }
};

$.representCountryButtonGroup = function (container, data) {
    $.each(data, function (i, p) {
        $(container).append($('<button class="btn btn-secondary"></button>').val(p.Country).html(p.Country));
    });
    $(container).prepend($('<button class="btn btn-secondary active"></button>').val('--ALL--').html('--ALL--'));

    $('.btn-secondary').on('click', function () {
        $('.btn-secondary').removeClass('active');
        $(this).addClass('active');
        var country = null;
        if ($(this).val() !== '--ALL--' && $(this).val())
            country = $(this).val();
        if ($(optsStats).hasClass('active'))
            $.prepareAnnotationData(country, 'opts');
        else
            $.prepareAnnotationData(country, 'revs');
    });
};
// This function shows dropdown of countries, getting the data dynamically from the database
$.representCountryDropdown = function (container, data) {
    $.each(data, function (i, p) {
        $('#' + container).append($('<option></option>').val(p.Country).html(p.Country));
    });
    if (Cookies.get('Country')) {
        $('#' + container).val(Cookies.get('Country'));
        Cookies.remove('Country');
    }
};
// This function shows dropdown of operator, depending on country selected, getting the data dynamically from the database
$.representOperatorDropdown = function (container, data) {

    var containerSelector = $('#' + container);
    containerSelector.find('option').remove();
    containerSelector.append($('<option></option>').val("--ALL--").html("--ALL--"));
    $.each(data, function (i, p) {
        $('#' + container).append($('<option></option>').val(p.Operator).html(p.Operator));
    });
};
// This function shows dropdown of services, depending on operator selected, getting the data dynamically from the database
$.representServicesDropdown = function (container, data) {
    var d = data.reduceRight(function (r, a) {
        r.some(function (b) {
            return a.Service === b.Service;
        }) || r.push(a);
        return r;
    }, []);
    var containerSelector = $('#' + container);
    containerSelector.find('option').remove();
    containerSelector.append($('<option></option>').val("--ALL--").html("--ALL--"));
    $.each(d, function (i, p) {
        $('#' + container).append($('<option></option>').val(p.Service).html(p.Service));
    });
};
// This function appends pie chart to container
$.representPieChart = function (container, data) {
    var d = [];
    for (var i = 0; i < data.length; i++) {
        d.push({
            label: data[i].Service,
            y: parseFloat(data[i].Revenue)
        });
    }
    buildPieChart(container, d, "Revenue per Service for " + pieChartOp);
};
// This function appends the opts chart to the container
$.representOptsChart = function (container, data, representation) {
    var d1 = [];
    var d2 = [];
    var d3 = [];
    if (representation === 'optsChartService') {
        for (var i = 0; i < data.length; i++) {
            d1.push({
                label: data[i].Service,
                y: parseInt(data[i]["Total Optins"])
            });
            d2.push({
                label: data[i].Service,
                y: parseInt(data[i]["Total Optouts"])
            });
            d3.push({
                label: data[i].Service,
                y: parseInt(data[i]["Total Optfails"])
            });
        }
    } else {
        for (var i = 0; i < data.length; i++) {
            d1.push({
                label: data[i].Operator,
                y: parseInt(data[i]["Total Optins"])
            });
            d2.push({
                label: data[i].Operator,
                y: parseInt(data[i]["Total Optouts"])
            });
            d3.push({
                label: data[i].Operator,
                y: parseInt(data[i]["Total Optfails"])
            });
        }
    }
    buildOptGraph(container, d1, d2, d3, 'column', 'Total Opt ins/outs/fails', 'Operator', 'Opts');
};

// Function to actually build the chart using CanvasJs
function buildChart(div, data, type, title, xtitle, ytitle) {

    $('#' + clickCoordinatesTable).empty();
    chart2 = new CanvasJS.Chart(div,
        {
            //animationEnabled: true,
            //animationDuration: 500,
            backgroundColor: "transparent",
            zoomEnabled: true,
            colorSet: "revenuesColorSet",
            title: {
                text: title
            },
            axisX: {
                interval: 1,
                labelAngle: -35,
                labelFontSize: 14,
                title: xtitle,
                viewportMinimum: null,
                viewportMaximum: null
            },
            axisY: {
                title: ytitle,
                includeZero: true
            },
            data: [{
                type: type,
                click: onClick,
                dataPoints: data  // random generator below
            }]
        });

    chart2.render();
    $('.canvasjs-chart-toolbar button:eq(0)').text('Pan').addClass('myButton').click(function () {
        if ($(this).hasClass("clicked")) {
            $(this).text("Pan").removeClass("clicked");
        } else {
            $(this).text("Zoom").addClass("clicked");
        }
    });
    $('.canvasjs-chart-toolbar button:eq(1)').text('Restore').addClass('myButton').click(function () {
        $('.canvasjs-chart-toolbar button:eq(0)').text('Pan').removeClass("clicked");
    });

    // Function that shows extra information upon clicking a data point
    function onClick(e) {
        if (!($(optInsButton).hasClass('active'))
            && !($(optOutsButton).hasClass('active'))
            && !($(optFailsButton).hasClass('active'))) {
            var operator = $('#' + selectOperator).val();
            var fDate = $('#fromDate').val();
            var tDate = $('#toDate').val();
            pieChartOp = e.dataPoint.label;
            if (operator === "--ALL--" || !operator) {
                if (fDate === "" && tDate === "") {
                    $.sendRequest(requests[9], clickCoordinatesTable, representation[9], null, null, null, e.dataPoint.label);
                } else {
                    $.sendRequest(requests[9], clickCoordinatesTable, representation[9], fDate, tDate, null, e.dataPoint.label);
                }
                $('html, body').animate({scrollTop: $('#' + clickCoordinatesTable).offset().top}, 500);
            }

            $("#myInput").show();
        }

    }

    document.getElementById("exportChart").addEventListener("click", function () {
        chart2.exportChart({format: "jpg", fileName: 'revenue graph'});
    });
}

// Function that actually builds the pie chart using CanvasJs
function buildPieChart(container, data, title) {
    var chart3 = new CanvasJS.Chart(container, {
        animationEnabled: true,
        animationDuration: 700,
        colorSet: "pieColorSet",
        backgroundColor: "#f4f4f4",
        title: {
            text: title
        },
        data: [{
            type: "pie",
            startAngle: 0,
            yValueFormatString: "##0.00",
            indexLabel: "{label}-#percent%-{y}",
            dataPoints: data
        }]
    });
    chart3.render();
}

// Function to convert date to needed format
function convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString();
    var dd = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]) + '-' + (ddChars[1] ? dd : "0" + ddChars[0]);
}

// This function builds the Opt Graph using CanvasJs
function buildOptGraph(div, data, data2, data3, type, title, xtitle, ytitle) {
    $('#' + clickCoordinatesTable).empty();
    var chart = new CanvasJS.Chart(div,
        {
            backgroundColor: "#f4f4f4",
            zoomEnabled: true,
            colorSet: "optsColorSet",
            title: {
                text: title
            },
            legend: {
                cursor: "pointer",
                itemclick: function (e) {
                    console.log("legend click: " + e.dataPointIndex);
                    console.log(e);
                    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    } else {
                        e.dataSeries.visible = true;
                    }

                    e.chart.render();
                }
            },
            axisX: {
                interval: 1,
                labelAngle: -35,
                labelFontSize: 14,
                title: xtitle,
                viewportMinimum: null,
                viewportMaximum: null
            },
            axisY: {
                title: ytitle,
                includeZero: true
            },
            data: [{
                showInLegend: true,
                type: type,
                click: onClick,
                dataPoints: data,
                legendText: "Opt Ins"
            },
                {
                    showInLegend: true,
                    type: type,
                    click: onClick,
                    dataPoints: data2,
                    legendText: "Opt Outs"
                },
                {
                    showInLegend: true,
                    type: type,
                    click: onClick,
                    dataPoints: data3,
                    legendText: "Opt Fails"
                }
            ]
        });


    chart.render();
    $('.canvasjs-chart-toolbar button:eq(0)').text('Pan').addClass('myButton').click(function () {
        if ($(this).hasClass("clicked")) {
            $(this).text("Pan").removeClass("clicked");
        } else {
            $(this).text("Zoom").addClass("clicked");
        }
    });
    $('.canvasjs-chart-toolbar button:eq(1)').text('Restore').addClass('myButton').click(function () {
        $('.canvasjs-chart-toolbar button:eq(0)').text('Pan').removeClass("clicked");
    });

    // This function shows more information upon clicking a data point
    function onClick(e) {
        var operator = $('#' + selectOperator).val();
        var fDate = $('#fromDate').val();
        var tDate = $('#toDate').val();
        pieChartOp = e.dataPoint.label;
        if (operator === "--ALL--" || !operator) {
            if (fDate === "" && tDate === "") {
                $.sendRequest(requests[7], '#' + clickCoordinatesTable, representation[0], null, null, null, e.dataPoint.label);
            } else {
                $.sendRequest(requests[7], '#' + clickCoordinatesTable, representation[0], fDate, tDate, null, e.dataPoint.label);
            }
            $('html, body').animate({scrollTop: $('#' + clickCoordinatesTable).offset().top}, 500);
        }

    }

    document.getElementById("exportChart").addEventListener("click", function () {
        chart.exportChart({format: "jpg", fileName: 'revenue graph'});
    });
}


