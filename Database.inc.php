<?php

class Database_r
{

    public $host = 'localhost';

    public $user = 'root';

    public $password = '';

    public $dbname = 'mock_input_db';

    public $pdo;

    //returns a PDO Connection Object used for SQL commands
    public function getConnection()
    {
        // Create PDO Instance
        try {
            $this->pdo = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->dbname, $this->user, $this->password);

            // Remeber to switch error mod off when done

            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->exec("set names utf8");
            //echo "Connected! <br>";
        } catch (PDOException $e) {
            $error = array(
                'status' => "Failed",
                "msg" => "Could not connect!"
            );
            $this->response($this->json($error), 404);
        }
        return $this->pdo;
    }
}

?>