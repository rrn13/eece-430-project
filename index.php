<!--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! HTML & PHP INDEX PAGE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<!--Bootstrap link-->
	<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" rel="stylesheet">
	<!--Font Awesome link-->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<!--Custom Style Sheet Link-->
	<link href="resources/style.css" rel="stylesheet">
	<!--JQuery CDN-->
	<!--Make sure not to use the slim version as it does not contain ajax method-->
	<script src="js/jquery-3.3.1.js"></script>
	<!--PopperJS CDN-->
	<script crossorigin="anonymous"
	        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<!--jsPDF Library-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
	<!--Table Export Library-->
	<script src="js/tableHTMLExport.js"></script>
	<!--Bootstrap CDN-->
	<!--important: insert JQuery CDN before-->
	<script crossorigin="anonymous"
	        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	        src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<!--JS file-->
	<!--important: insert JQuery CDN before-->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
	<script src="https://www.gstatic.com/charts/loader.js" type="text/javascript"></script>
	<script src="js/canvasjs.min.js" type="text/javascript"></script>
	<script src="JQuery-Layering.js"></script>
	<title>Home</title>
</head>
<body style="font-size: 0.9em">
<div id="includedContent"></div>
<script>
    // Add NavBar
    $("#includedContent").load("navBar.html", function () {
        $('.nav a.active').removeClass('bg-secondary');
        $(homeNav).addClass('bg-secondary');
        $(homeNav).addClass('text-white');
    });
</script>
<br>

<div class="container-fluid d-flex flex-column ">
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<button id="countries-table-view" class="nav-link active" type="button"
			        style="padding-right: 20px; padding-left: 20px; margin-right: 20px; margin-left: 20px"><i
						class="fa fa-table" aria-hidden="true"></i> Table View
			</button>
		</li>
		<li class="nav-item">
			<button id="countries-map-view" class="nav-link" type="button"
			        style="padding-right: 20px; padding-left: 20px"><i class="fa fa-map-marker" aria-hidden="true"></i>
				Map View
			</button>
		</li>
	</ul>
	<!--
	<div class="d-flex flex-column" style="width: 20%">

            <div class="d-flex flex-row p-2">
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input" id="radioTable" name="inlineMaterialRadiosExample">
                    <label class="form-check-label" for="materialInline1">Table</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input" id="radioLine" name="inlineMaterialRadiosExample">
                    <label class="form-check-label" for="materialInline2">Line Graph</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input" id="radioBar" name="inlineMaterialRadiosExample">
                    <label class="form-check-label" for="materialInline3">Bar Graph</label>
                </div>
            </div>
</div>-->
	<hr>
	<div id="countries-data-container" style="margin-right: 20px; margin-left: 20px"></div>
</div>
<script>
    history.pushState(null, null, 'index.php');
    window.addEventListener('popstate', function () {
        history.pushState(null, null, 'index.php');
    });
</script>
</body>
</html>

