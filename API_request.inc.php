<?php

class API extends REST
{

    private $db = null;

    private $timeout = 60000;

    // -----------------------------------------------Initialize-----------------------------------------------

    // Calls REST constructor
    // Sets up connection through database class
    // db is a Connection object used to execute PDO commands
    public function __construct()
    {
        parent::__Construct();

        $database = new Database_r();
        $this->db = $database->getConnection();
    }

    // Main function for receiving requests when API is called
    // takes as input:
    // request = <function name>
    // username = <admin>
    // password = <artail>
    // any extra parameters according to called function (check documentation)
    public function processAPI()
    {
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['request'])));
        $func = str_replace("-", "_", $func);
        // $func = 'getTodayRevenue';
        // $func = 'getCurrency';
        // echo $func;
        if ((int)method_exists($this, $func)) {
            $this->$func();
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Method not found"
            );
            $this->response($this->json($error), 404);
        }
    }

    // Encodes data in JSON format, used by response function
    private function json($data)
    {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    // get response from client
    function getresponse($link)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $link);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    // Hexadecimal to Binary
    function local_hex2bin($h)
    {
        if (!is_string($h))
            return null;
        $r = '';
        for ($a = 0; $a < strlen($h); $a += 2) {
            $r .= chr(hexdec($h{$a} . $h{($a + 1)}));
        }
        return $r;
    }

    // To avoid SQL Injections
    function removeSlash($string)
    {
        $string = str_replace("\\", "\\\\", $string);
        $string = str_replace("'", "\'", $string);
        $string = str_replace('"', '\"', $string);
        return $string;
    }

    // Returns Client/Requestor IP
    function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    // ----------------------------------------------FUNCTIONS------------------------------------------------
    private function login()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            // $this->db=NULL;
            $this->response($this->json($error), 400);
        } elseif (!isset($this->_request['username'], $this->_request['password'])) {
            $error = array(
                'status' => "Failed",
                "msg" => "Wrong Parameters, Please enter correct parameters."
            );
            // $this->db=NULL;
            $this->response($this->json($error), 400);
        } else {

            $username = $this->_request['username'];
            $query = "select * from users where username='" . $username . "'";

            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {
                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {
                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {
                $this->response('User Not Found', 200);
            } else {
                $userdata = $select_query->fetch(PDO::FETCH_ASSOC);

                if ($this->_request['password'] != $userdata['password']) {
                    $error = array(
                        'status' => "Failed",
                        "msg" => "Invalid Password!"
                    );
                    $this->response($this->json($error), 401);
                } else {
                    $_SESSION['username'] = $this->_request['username'];
                    $_SESSION['password'] = $this->_request['password'];
                    $_SESSION['login_time'] = time();
                    $_SESSION['logged_in'] = TRUE;
                    $message = array(
                        'Login Successful!'
                    );
                    $this->response($this->json($message), 200);
                }
            }
        }
    }

    private function logout()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            // $this->db=NULL;
            $this->response($this->json($error), 400);
        } else {
            session_destroy();
            $message = array(
                'Logout Successful!'
            );
            $this->response($this->json($message), 200);
        }
    }

    private function register()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            // $this->db=NULL;
            $this->response($this->json($error), 400);
        } elseif (!isset($this->_request['username'], $this->_request['password'])) {
            $error = array(
                'status' => "Failed",
                "msg" => "Wrong Parameters, Please enter correct parameters."
            );
            // $this->db=NULL;
            $this->response($this->json($error), 400);
        } else {
            $username = $this->_request['username'];
            $password = $this->_request['password'];
            $query = "INSERT INTO users VALUES ('" . $username . "', '" . $password . "')";

            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {
                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {
                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }
            $message = array(
                'User Registered Successfully!'
            );
            $this->response($this->json($message), 200);
        }
    }

    // Returns Operators Revenue Table
    private function getOperatorsRevenue()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['country']) && !isset($this->_request['operator'])) {
            $query = "SELECT temp.Operator, temp.Country, SUM(temp.Revenue) AS Revenue FROM
                        (SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name GROUP BY Oin.service) AS temp
                        GROUP BY temp.Operator";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['country']) && !isset($this->_request['operator'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $query = "SELECT temp.Operator, temp.Country, SUM(temp.Revenue) AS Revenue FROM
                        (SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' GROUP BY Oin.service) AS temp
                        GROUP BY temp.Operator";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date'], $this->_request['end_date'], $this->_request['country']) && !isset($this->_request['operator'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $country = $this->_request['country'];
            $query = "SELECT temp.Operator, temp.Country, SUM(temp.Revenue) AS Revenue FROM
                        (SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND Op.country='" . $country . "' AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' GROUP BY Oin.service) AS temp
                        GROUP BY temp.Operator";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['country']) && isset($this->_request['operator'])) {
            $operator = $this->_request['operrator'];
            $query = "SELECT temp.Operator, temp.Country, SUM(temp.Revenue) AS Revenue FROM
                        (SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND Op.operator_name='" . $operator . "'  GROUP BY Oin.service) AS temp
                        GROUP BY temp.Operator";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && isset($this->_request['country']) && !isset($this->_request['operator'])) {
            $country = $this->_request['country'];
            $query = "SELECT temp.Operator, temp.Country, SUM(temp.Revenue) AS Revenue FROM
                        (SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND Op.country='" . $country . "'  GROUP BY Oin.service) AS temp
                        GROUP BY temp.Operator";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['country']) && isset($this->_request['operator'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $query = "SELECT temp.Operator, temp.Country, SUM(temp.Revenue) AS Revenue FROM
                        (SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND Op.country='" . $country . "' AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' GROUP BY Oin.service) AS temp
                        GROUP BY temp.Operator";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Enter Valid Parameters"
            );
            $this->response($this->json($error), 400);
        }
    }

    // Returns Services Revenue Table
    private function getServicesRevenue()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['country']) && !isset($this->_request['operator'])) {
            $query = "SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name GROUP BY Oin.service";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['country']) && !isset($this->_request['operator'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $query = "SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' GROUP BY Oin.service";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date'], $this->_request['end_date'], $this->_request['country']) && !isset($this->_request['operator'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $country = $this->_request['country'];
            $query = "SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND Op.country='" . $country . "' AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' GROUP BY Oin.service";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && isset($this->_request['country']) && !isset($this->_request['operator'])) {
            $country = $this->_request['country'];
            $query = "SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND Op.country='" . $country . "' GROUP BY Oin.service";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['country']) && isset($this->_request['operator'])) {
            $operator = $this->_request['operator'];
            $query = "SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND S.operator='" . $operator . "' GROUP BY Oin.service";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date'], $this->_request['end_date'], $this->_request['operator']) && !isset($this->_request['country'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $query = "SELECT S.service_Name AS Service, S.operator AS Operator, Op.country AS Country, COUNT(Oin.optin_ID)*S.tariff AS Revenue FROM Service S, optins Oin, operator Op WHERE Oin.service=S.service_ID AND S.operator=Op.operator_name AND S.operator='" . $operator . "' AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' GROUP BY Oin.service";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Enter Valid Start and End Dates"
            );
            $this->response($this->json($error), 400);
        }
    }

    // Returns Country Table
    private function getCountry()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } else {

            $query = "SELECT * FROM country";
            // echo $query; exit;
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        }
    }

    // Returns Operator Table
    private function getOperators()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (isset($this->_request['country'])) {
            $country = $this->_request['country'];
            $query = "SELECT operator_name AS Operator FROM operator WHERE country='" . $country . "'";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $query = "SELECT operator_name AS Operator, country AS Country FROM operator";
            // echo $query; exit;
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        }
    }

    // Returns Service table
    private function getServices()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (isset($this->_request['operator']) && !isset($this->_request['country'])) {
            $operator = $this->_request['operator'];
            $query = "SELECT S.service_name AS Service, S.operator AS Operator, Op.country AS Country, S.short_code AS 'Short Code', S.tariff AS Tariff FROM service S, operator Op WHERE S.operator=Op.operator_name AND S.operator='" . $operator . "'";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['operator']) && isset($this->_request['country'])) {
            $country = $this->_request['country'];
            $query = "SELECT S.service_name AS Service, S.operator AS Operator, Op.country AS Country, S.short_code AS 'Short Code', S.tariff AS Tariff FROM service S, operator Op WHERE S.operator=Op.operator_name AND Op.country='" . $country . "'";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['operator']) && isset($this->_request['country'])) {
            $country = $this->_request['country'];
            $operator = $this->_request['operator'];
            $query = "SELECT S.service_name AS Service, S.operator AS Operator, Op.country AS Country, S.short_code AS 'Short Code', S.tariff AS Tariff FROM service S, operator Op WHERE S.operator=Op.operator_name AND Op.country='" . $country . "' AND Op.operator_name='" . $operator . "'";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $operator = $this->_request['operator'];
            $query = "SELECT S.service_name AS Service, S.operator AS Operator, Op.country AS Country, S.short_code AS 'Short Code', S.tariff AS Tariff FROM service S, operator Op WHERE S.operator=Op.operator_name";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        }
    }

    // returns Optin table
    private function getOptins()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $query = "SELECT O.optin_ID AS ID, O.operator AS Operator, S.service_name AS Service, O.method AS Method, O.date AS Date FROM Optins O, Service S WHERE O.service=S.service_ID AND O.date>'" . $startdate . "' AND O.date<'" . $enddate . "';";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date'])) {
            $query = "SELECT O.optin_ID AS ID, O.operator AS Operator, S.service_name AS Service, O.method AS Method, O.date AS Date FROM Optins O, Service S WHERE O.service=S.service_ID;";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Enter Valid Start and End Dates"
            );
            $this->response($this->json($error), 400);
        }
    }

    // returns Optout table
    private function getOptouts()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $query = "SELECT O.optout_ID AS ID, O.operator AS Operator, S.service_name AS Service, O.date AS 'Date'  FROM optouts O, Service S WHERE O.service=S.service_ID AND O.date>'" . $startdate . "' AND O.date<'" . $enddate . "';";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date'])) {
            $query = "SELECT O.optout_ID AS ID, O.operator AS Operator, S.service_name AS Service, O.date AS 'Date'  FROM optouts O, Service S WHERE O.service=S.service_ID";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Enter Valid Start and End Dates"
            );
            $this->response($this->json($error), 400);
        }
    }

    // returns Optfail table
    private function getOptfails()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $query = "SELECT O.optfail_ID AS ID, O.operator AS Operator, S.service_name AS Service, O.reason AS Reason, O.date AS 'Date'  FROM optfail O, Service S WHERE O.service=S.service_ID AND O.date>'" . $startdate . "' AND O.date<'" . $enddate . "';";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date'])) {
            $query = "SELECT O.optfail_ID AS ID, O.operator AS Operator, S.service_name AS Service, O.reason AS Reason, O.date AS 'Date'  FROM optfail O, Service S WHERE O.service=S.service_ID";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Enter Valid Start and End Dates"
            );
            $this->response($this->json($error), 400);
        }
    }

    //Returns Charging Rate for Services
    private function getChargingRate()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['country']) && !isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $query = "SELECT S.service_name AS Service, S.short_code AS 'Short Code', S.tariff AS Tariff, S.operator As Operator, COUNT(Ofail.optfail_ID)+tempoutin.total_in_out AS 'Total Opts', tempoutin.Successful_Optins AS 'Successful Optins',
                        (tempoutin.Successful_Optins/(COUNT(Ofail.optfail_ID)+tempoutin.total_in_out))*100 AS 'Charging Rate'
                        FROM service S, optfail Ofail,
                        (Select S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oin.optin_ID)+tempout.total_out As total_in_out, COUNT(Oin.optin_ID) AS Successful_Optins
                        FROM service S, optins Oin,
                        (SELECT S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oout.optout_ID) AS total_out FROM Service S, optouts Oout 
                        WHERE Oout.service=S.service_ID GROUP BY S.service_ID) AS tempout
                        WHERE S.service_ID=tempout.service_ID AND S.service_ID=Oin.service GROUP BY S.service_ID) AS tempoutin
                        WHERE S.service_ID=tempoutin.service_ID AND S.service_ID=Ofail.service GROUP BY S.service_ID;";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['country']) && !isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $query = "SELECT S.service_name AS Service, S.short_code AS 'Short Code', S.tariff AS Tariff, S.operator As Operator, COUNT(Ofail.optfail_ID)+tempoutin.total_in_out AS 'Total Opts', tempoutin.Successful_Optins AS 'Successful Optins',
                        (tempoutin.Successful_Optins/(COUNT(Ofail.optfail_ID)+tempoutin.total_in_out))*100 AS 'Charging Rate'
                        FROM service S, optfail Ofail,
                        (Select S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oin.optin_ID)+tempout.total_out As total_in_out, COUNT(Oin.optin_ID) AS Successful_Optins
                        FROM service S, optins Oin,
                        (SELECT S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oout.optout_ID) AS total_out FROM Service S, optouts Oout 
                        WHERE Oout.service=S.service_ID AND Oout.date>'" . $startdate . "' AND Oout.date<'" . $enddate . "' GROUP BY S.service_ID) AS tempout
                        WHERE S.service_ID=tempout.service_ID AND S.service_ID=Oin.service AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "'  GROUP BY S.service_ID) AS tempoutin
                        WHERE S.service_ID=tempoutin.service_ID AND S.service_ID=Ofail.service AND Ofail.date>'" . $startdate . "' AND Ofail.date<'" . $enddate . "' 
                        GROUP BY S.service_ID;";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && isset($this->_request['country']) && !isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $country = $this->_request['country'];
            $query = "SELECT S.service_name AS Service, S.short_code AS 'Short Code', S.tariff AS Tariff, S.operator As Operator, COUNT(Ofail.optfail_ID)+tempoutin.total_in_out AS 'Total Opts', tempoutin.Successful_Optins AS 'Successful Optins',
                        (tempoutin.Successful_Optins/(COUNT(Ofail.optfail_ID)+tempoutin.total_in_out))*100 AS 'Charging Rate'
                        FROM service S, optfail Ofail, operator Op,
                            (Select S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oin.optin_ID)+tempout.total_out As total_in_out, COUNT(Oin.optin_ID) AS Successful_Optins
                            FROM service S, optins Oin,operator Op,
                            (SELECT S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oout.optout_ID) AS total_out FROM Service S, optouts Oout, operator Op
                            WHERE Oout.service=S.service_ID AND Oout.operator=Op.operator_name AND Op.country='" . $country . "' GROUP BY S.service_ID) AS tempout
                            WHERE S.service_ID=tempout.service_ID AND S.service_ID=Oin.service AND tempout.operator=Op.operator_name AND Op.country='" . $country . "' GROUP BY S.service_ID) AS tempoutin
                            WHERE S.service_ID=tempoutin.service_ID AND S.service_ID=Ofail.service AND tempoutin.operator=Op.operator_name AND Op.country='" . $country . "' GROUP BY S.service_ID;";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && isset($this->_request['country']) && !isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $country = $this->_request['country'];
            $query = "SELECT S.service_name AS Service, S.short_code AS 'Short Code', S.tariff AS Tariff, S.operator As Operator, COUNT(Ofail.optfail_ID)+tempoutin.total_in_out AS 'Total Opts', tempoutin.Successful_Optins AS 'Successful Optins',
                        (tempoutin.Successful_Optins/(COUNT(Ofail.optfail_ID)+tempoutin.total_in_out))*100 AS 'Charging Rate'
                        FROM service S, optfail Ofail, operator Op,
                            (Select S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oin.optin_ID)+tempout.total_out As total_in_out, COUNT(Oin.optin_ID) AS Successful_Optins
                            FROM service S, optins Oin,operator Op,
                            (SELECT S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oout.optout_ID) AS total_out FROM Service S, optouts Oout, operator Op
                            WHERE Oout.service=S.service_ID AND Oout.operator=Op.operator_name AND Oout.date>'" . $startdate . "' AND Oout.date<'" . $enddate . "' AND Op.country='" . $country . "' GROUP BY S.service_ID) AS tempout
                            WHERE S.service_ID=tempout.service_ID AND S.service_ID=Oin.service AND tempout.operator=Op.operator_name AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' AND Op.country='" . $country . "' GROUP BY S.service_ID) AS tempoutin
                            WHERE S.service_ID=tempoutin.service_ID AND S.service_ID=Ofail.service AND tempoutin.operator=Op.operator_name AND Ofail.date>'" . $startdate . "' AND Ofail.date<'" . $enddate . "' AND Op.country='" . $country . "' GROUP BY S.service_ID;";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['country']) && isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $query = "SELECT S.service_name AS Service, S.short_code AS 'Short Code', S.tariff AS Tariff, S.operator As Operator, COUNT(Ofail.optfail_ID)+tempoutin.total_in_out AS 'Total Opts', tempoutin.Successful_Optins AS 'Successful Optins',
                        (tempoutin.Successful_Optins/(COUNT(Ofail.optfail_ID)+tempoutin.total_in_out))*100 AS 'Charging Rate'
                        FROM service S, optfail Ofail, operator Op,
                            (Select S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oin.optin_ID)+tempout.total_out As total_in_out, COUNT(Oin.optin_ID) AS Successful_Optins
                            FROM service S, optins Oin,operator Op,
                            (SELECT S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oout.optout_ID) AS total_out FROM Service S, optouts Oout, operator Op
                            WHERE Oout.service=S.service_ID AND Oout.operator=Op.operator_name AND Oout.date>'" . $startdate . "' AND Oout.date<'" . $enddate . "' AND S.operator='" . $operator . "' GROUP BY S.service_ID) AS tempout
                            WHERE S.service_ID=tempout.service_ID AND S.service_ID=Oin.service AND tempout.operator=Op.operator_name AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' AND S.operator='" . $operator . "' GROUP BY S.service_ID) AS tempoutin
                            WHERE S.service_ID=tempoutin.service_ID AND S.service_ID=Ofail.service AND tempoutin.operator=Op.operator_name AND Ofail.date>'" . $startdate . "' AND Ofail.date<'" . $enddate . "' AND S.operator='" . $operator . "' GROUP BY S.service_ID;
            ";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['country']) && isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $query = "SELECT S.service_name AS Service, S.short_code AS 'Short Code', S.tariff AS Tariff, S.operator As Operator, COUNT(Ofail.optfail_ID)+tempoutin.total_in_out AS 'Total Opts', tempoutin.Successful_Optins AS 'Successful Optins',
                        (tempoutin.Successful_Optins/(COUNT(Ofail.optfail_ID)+tempoutin.total_in_out))*100 AS 'Charging Rate'
                        FROM service S, optfail Ofail, operator Op,
                            (Select S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oin.optin_ID)+tempout.total_out As total_in_out, COUNT(Oin.optin_ID) AS Successful_Optins
                            FROM service S, optins Oin,operator Op,
                            (SELECT S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oout.optout_ID) AS total_out FROM Service S, optouts Oout, operator Op
                            WHERE Oout.service=S.service_ID AND Oout.operator=Op.operator_name  AND S.operator='" . $operator . "' GROUP BY S.service_ID) AS tempout
                            WHERE S.service_ID=tempout.service_ID AND S.service_ID=Oin.service AND tempout.operator=Op.operator_name  AND S.operator='" . $operator . "' GROUP BY S.service_ID) AS tempoutin
                            WHERE S.service_ID=tempoutin.service_ID AND S.service_ID=Ofail.service AND tempoutin.operator=Op.operator_name AND S.operator='" . $operator . "' GROUP BY S.service_ID;
            ";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['country']) && isset($this->_request['operator']) && isset($this->_request['service'])) {
            $service = $this->_request['service'];
            $operator = $this->_request['operator'];
            $query = "SELECT S.service_name AS Service, S.short_code AS 'Short Code', S.tariff AS Tariff, S.operator As Operator, COUNT(Ofail.optfail_ID)+tempoutin.total_in_out AS 'Total Opts', tempoutin.Successful_Optins AS 'Successful Optins',
                        (tempoutin.Successful_Optins/(COUNT(Ofail.optfail_ID)+tempoutin.total_in_out))*100 AS 'Charging Rate'
                        FROM service S, optfail Ofail, operator Op,
                            (Select S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oin.optin_ID)+tempout.total_out As total_in_out, COUNT(Oin.optin_ID) AS Successful_Optins
                            FROM service S, optins Oin,operator Op,
                            (SELECT S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oout.optout_ID) AS total_out FROM Service S, optouts Oout, operator Op
                            WHERE Oout.service=S.service_ID AND Oout.operator=Op.operator_name  AND S.operator='" . $operator . "' AND S.service_name='" . $service . "' GROUP BY S.service_ID) AS tempout
                            WHERE S.service_ID=tempout.service_ID AND S.service_ID=Oin.service AND tempout.operator=Op.operator_name  AND S.operator='" . $operator . "' AND S.service_name='" . $service . "' GROUP BY S.service_ID) AS tempoutin
                            WHERE S.service_ID=tempoutin.service_ID AND S.service_ID=Ofail.service AND tempoutin.operator=Op.operator_name AND S.operator='" . $operator . "' AND S.service_name='" . $service . "' GROUP BY S.service_ID;
            ";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['country']) && isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $service = $this->_request['service'];
            $query = "SELECT S.service_name AS Service, S.short_code AS 'Short Code', S.tariff AS Tariff, S.operator As Operator, COUNT(Ofail.optfail_ID)+tempoutin.total_in_out AS 'Total Opts', tempoutin.Successful_Optins AS 'Successful Optins',
                        (tempoutin.Successful_Optins/(COUNT(Ofail.optfail_ID)+tempoutin.total_in_out))*100 AS 'Charging Rate'
                        FROM service S, optfail Ofail, operator Op,
                            (Select S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oin.optin_ID)+tempout.total_out As total_in_out, COUNT(Oin.optin_ID) AS Successful_Optins
                            FROM service S, optins Oin,operator Op,
                            (SELECT S.service_ID, S.service_name, S.short_code, S.tariff, S.operator, COUNT(Oout.optout_ID) AS total_out FROM Service S, optouts Oout, operator Op
                            WHERE Oout.service=S.service_ID AND Oout.operator=Op.operator_name AND Oout.date>'" . $startdate . "' AND Oout.date<'" . $enddate . "' AND S.operator='" . $operator . "' AND S.service_name='" . $service . "' GROUP BY S.service_ID) AS tempout
                            WHERE S.service_ID=tempout.service_ID AND S.service_ID=Oin.service AND tempout.operator=Op.operator_name AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' AND S.operator='" . $operator . "' AND S.service_name='" . $service . "' GROUP BY S.service_ID) AS tempoutin
                            WHERE S.service_ID=tempoutin.service_ID AND S.service_ID=Ofail.service AND tempoutin.operator=Op.operator_name AND Ofail.date>'" . $startdate . "' AND Ofail.date<'" . $enddate . "' AND S.operator='" . $operator . "' AND S.service_name='" . $service . "' GROUP BY S.service_ID;
            ";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Enter Valid Start and End Dates"
            );
            $this->response($this->json($error), 400);
        }
    }

    //Returns Opts according to parameters
    private function getOpts()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $query = "SELECT tempinout.service AS Service,tempinout.operator AS Operator,  total_ins AS 'Total Optins', total_outs AS 'Total Optouts', COUNT(ofail.optfail_ID) AS 'Total Optfails' FROM 
                        (SELECT tempin.service as service, tempin.operator, tempin.service_ID, total_ins, COUNT(oout.optout_ID) AS total_outs FROM
                        (SELECT op.operator_name as operator, S.service_name AS service, S.service_ID AS service_ID, COUNT(oin.optin_ID) as total_ins FROM operator op, service S, optins oin 
                        WHERE S.operator=op.operator_name AND S.service_ID=oin.service AND oin.date>'" . $startdate . "' AND oin.date<'" . $enddate . "'   GROUP BY S.service_ID) AS tempin, optouts oout 
                        WHERE tempin.service_ID=oout.service AND oout.date>'" . $startdate . "' AND oout.date<'" . $enddate . "' GROUP BY tempin.service_ID) AS tempinout, optfail ofail
                        WHERE tempinout.service_ID=ofail.service AND ofail.date>'" . $startdate . "' AND ofail.date<'" . $enddate . "' GROUP BY tempinout.service_ID";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $query = "SELECT tempinout.service AS Service,tempinout.operator AS Operator,  total_ins AS 'Total Optins', total_outs AS 'Total Optouts', COUNT(ofail.optfail_ID) AS 'Total Optfails' FROM 
                        (SELECT tempin.service as service, tempin.operator, tempin.service_ID, total_ins, COUNT(oout.optout_ID) AS total_outs FROM
                        (SELECT op.operator_name as operator, S.service_name AS service, S.service_ID AS service_ID, COUNT(oin.optin_ID) as total_ins FROM operator op, service S, optins oin 
                        WHERE S.operator=op.operator_name AND S.service_ID=oin.service AND oin.date>'" . $startdate . "' AND oin.date<'" . $enddate . "' AND op.operator_name='" . $operator . "'  GROUP BY S.service_ID) AS tempin, optouts oout 
                        WHERE tempin.service_ID=oout.service AND oout.date>'" . $startdate . "' AND oout.date<'" . $enddate . "' GROUP BY tempin.service_ID) AS tempinout, optfail ofail
                        WHERE tempinout.service_ID=ofail.service AND ofail.date>'" . $startdate . "' AND ofail.date<'" . $enddate . "' GROUP BY tempinout.service_ID";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $operator = $this->_request['operator'];
            $query = "SELECT tempinout.service AS Service,tempinout.operator AS Operator,  total_ins AS 'Total Optins', total_outs AS 'Total Optouts', COUNT(ofail.optfail_ID) AS 'Total Optfails' FROM 
                        (SELECT tempin.service as service, tempin.operator, tempin.service_ID, total_ins, COUNT(oout.optout_ID) AS total_outs FROM
                        (SELECT op.operator_name as operator, S.service_name AS service, S.service_ID AS service_ID, COUNT(oin.optin_ID) as total_ins FROM operator op, service S, optins oin 
                        WHERE S.operator=op.operator_name AND S.service_ID=oin.service AND op.operator_name='" . $operator . "'  GROUP BY S.service_ID) AS tempin, optouts oout 
                        WHERE tempin.service_ID=oout.service  GROUP BY tempin.service_ID) AS tempinout, optfail ofail
                        WHERE tempinout.service_ID=ofail.service  GROUP BY tempinout.service_ID";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && isset($this->_request['operator']) && isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $service = $this->_request['service'];
            $query = "SELECT tempinout.service AS Service,tempinout.operator AS Operator,  total_ins AS 'Total Optins', total_outs AS 'Total Optouts', COUNT(ofail.optfail_ID) AS 'Total Optfails' FROM 
                        (SELECT tempin.service as service, tempin.operator, tempin.service_ID, total_ins, COUNT(oout.optout_ID) AS total_outs FROM
                        (SELECT op.operator_name as operator, S.service_name AS service, S.service_ID AS service_ID, COUNT(oin.optin_ID) as total_ins FROM operator op, service S, optins oin 
                        WHERE S.operator=op.operator_name AND S.service_ID=oin.service AND oin.date>'" . $startdate . "' AND oin.date<'" . $enddate . "' AND op.operator_name='" . $operator . "' AND S.service_name='" . $service . "' GROUP BY S.service_ID) AS tempin, optouts oout 
                        WHERE tempin.service_ID=oout.service AND oout.date>'" . $startdate . "' AND oout.date<'" . $enddate . "' GROUP BY tempin.service_ID) AS tempinout, optfail ofail
                        WHERE tempinout.service_ID=ofail.service AND ofail.date>'" . $startdate . "' AND ofail.date<'" . $enddate . "' GROUP BY tempinout.service_ID";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && isset($this->_request['operator']) && isset($this->_request['service'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $service = $this->_request['service'];
            $query = "SELECT tempinout.service AS Service,tempinout.operator AS Operator,  total_ins AS 'Total Optins', total_outs AS 'Total Optouts', COUNT(ofail.optfail_ID) AS 'Total Optfails' FROM 
                        (SELECT tempin.service as service, tempin.operator, tempin.service_ID, total_ins, COUNT(oout.optout_ID) AS total_outs FROM
                        (SELECT op.operator_name as operator, S.service_name AS service, S.service_ID AS service_ID, COUNT(oin.optin_ID) as total_ins FROM operator op, service S, optins oin 
                        WHERE S.operator=op.operator_name AND S.service_ID=oin.service AND  op.operator_name='" . $operator . "' AND S.service_name='" . $service . "' GROUP BY S.service_ID) AS tempin, optouts oout 
                        WHERE tempin.service_ID=oout.service  GROUP BY tempin.service_ID) AS tempinout, optfail ofail
                        WHERE tempinout.service_ID=ofail.service  GROUP BY tempinout.service_ID";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['operator']) && !isset($this->_request['service'])) {
            $query = "SELECT tempinout.service AS Service, tempinout.operator AS Operator, total_ins AS 'Total Optins', total_outs AS 'Total Optouts', COUNT(ofail.optfail_ID) AS 'Total Optfails' FROM 
                        (SELECT tempin.service as service,tempin.operator as operator,  tempin.service_ID, total_ins, COUNT(oout.optout_ID) AS total_outs FROM
                        (SELECT  S.service_name AS service, op.operator_name as operator, S.service_ID AS service_ID, COUNT(oin.optin_ID) as total_ins FROM operator op, service S, optins oin 
                        WHERE S.operator=op.operator_name AND S.service_ID=oin.service  GROUP BY S.service_ID) AS tempin, optouts oout 
                        WHERE tempin.service_ID=oout.service  GROUP BY tempin.service_ID) AS tempinout, optfail ofail
                        WHERE tempinout.service_ID=ofail.service  GROUP BY tempinout.service_ID";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                // $json = json_encode($results);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Enter Valid Parameters!"
            );
            $this->response($this->json($error), 400);
        }
    }

    //Returns Total opts per operator
    private function getOptsOperators()
    {
        if ($this->get_request_method() != "POST") {
            $error = array(
                'status' => "Failed",
                "msg" => "Please send a POST request."
            );
            $this->response($this->json($error), 400);
        } elseif ($_SESSION['login_time'] == FALSE) {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Log In First"
            );
            $this->response($this->json($error), 400);
        } elseif (time() - $_SESSION['login_time'] > $this->timeout) {
            session_destroy();
            $error = array(
                'status' => "Failed",
                "msg" => "Session Timed Out, Please Log In Again"
            );
            $this->response($this->json($error), 400);
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['operator']) && !isset($this->_request['country'])) {
            $query = "SELECT Op.operator_name AS Operator, Op.country AS Country, tempinout.totaloptins AS 'Total Optins', tempinout.totaloptouts AS 'Total Optouts', COUNT(Ofail.optfail_ID) AS 'Total Optfails'
                        FROM operator Op, optfail Ofail,
                        (SELECT Op.operator_name AS operator, Op.country, tempin.totaloptins, COUNT(Oout.optout_ID) AS totaloptouts FROM operator Op, optouts Oout,
                        (SELECT Op.operator_name AS operator, Op.country AS country, COUNT(Oin.optin_ID) AS totaloptins FROM operator Op, optins Oin
                        WHERE Oin.operator=Op.operator_name GROUP BY Op.operator_name) AS tempin 
                        WHERE Op.operator_name=tempin.operator AND Op.operator_name=Oout.operator GROUP BY Op.operator_name) AS tempinout
                        WHERE Op.operator_name=tempinout.operator AND Op.operator_name=Ofail.operator GROUP BY Op.operator_name";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['operator']) && !isset($this->_request['country'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $query = "SELECT Op.operator_name AS Operator, Op.country AS Country, tempinout.totaloptins AS 'Total Optins', tempinout.totaloptouts AS 'Total Optouts', COUNT(Ofail.optfail_ID) AS 'Total Optfails'
                        FROM operator Op, optfail Ofail,
                        (SELECT Op.operator_name AS operator, Op.country, tempin.totaloptins, COUNT(Oout.optout_ID) AS totaloptouts FROM operator Op, optouts Oout,
                        (SELECT Op.operator_name AS operator, Op.country AS country, COUNT(Oin.optin_ID) AS totaloptins FROM operator Op, optins Oin
                        WHERE Oin.operator=Op.operator_name AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' GROUP BY Op.operator_name) AS tempin 
                        WHERE Op.operator_name=tempin.operator AND Op.operator_name=Oout.operator AND Oout.date>'" . $startdate . "' AND Oout.date<'" . $enddate . "' GROUP BY Op.operator_name) AS tempinout
                        WHERE Op.operator_name=tempinout.operator AND Op.operator_name=Ofail.operator AND Ofail.date>'" . $startdate . "' AND Ofail.date<'" . $enddate . "' GROUP BY Op.operator_name";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && isset($this->_request['operator']) && !isset($this->_request['country'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $operator = $this->_request['operator'];
            $query = "SELECT Op.operator_name AS Operator, Op.country AS Country, tempinout.totaloptins AS 'Total Optins', tempinout.totaloptouts AS 'Total Optouts', COUNT(Ofail.optfail_ID) AS 'Total Optfails'
                        FROM operator Op, optfail Ofail,
                        (SELECT Op.operator_name AS operator, Op.country, tempin.totaloptins, COUNT(Oout.optout_ID) AS totaloptouts FROM operator Op, optouts Oout,
                        (SELECT Op.operator_name AS operator, Op.country AS country, COUNT(Oin.optin_ID) AS totaloptins FROM operator Op, optins Oin
                        WHERE Oin.operator=Op.operator_name AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' AND Op.operator_name='" . $operator . "' GROUP BY Op.operator_name) AS tempin
                        WHERE Op.operator_name=tempin.operator AND Op.operator_name=Oout.operator AND Oout.date>'" . $startdate . "' AND Oout.date<'" . $enddate . "' GROUP BY Op.operator_name) AS tempinout
                        WHERE Op.operator_name=tempinout.operator AND Op.operator_name=Ofail.operator AND Ofail.date>'" . $startdate . "' AND Ofail.date<'" . $enddate . "' GROUP BY Op.operator_name";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && isset($this->_request['operator']) && !isset($this->_request['country'])) {
            $operator = $this->_request['operator'];
            $query = "SELECT Op.operator_name AS Operator, Op.country AS Country, tempinout.totaloptins AS 'Total Optins', tempinout.totaloptouts AS 'Total Optouts', COUNT(Ofail.optfail_ID) AS 'Total Optfails'
                        FROM operator Op, optfail Ofail,
                        (SELECT Op.operator_name AS operator, Op.country, tempin.totaloptins, COUNT(Oout.optout_ID) AS totaloptouts FROM operator Op, optouts Oout,
                        (SELECT Op.operator_name AS operator, Op.country AS country, COUNT(Oin.optin_ID) AS totaloptins FROM operator Op, optins Oin
                        WHERE Oin.operator=Op.operator_name AND Op.operator_name='" . $operator . "' GROUP BY Op.operator_name) AS tempin
                        WHERE Op.operator_name=tempin.operator AND Op.operator_name=Oout.operator GROUP BY Op.operator_name) AS tempinout
                        WHERE Op.operator_name=tempinout.operator AND Op.operator_name=Ofail.operator GROUP BY Op.operator_name";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (isset($this->_request['start_date']) && isset($this->_request['end_date']) && !isset($this->_request['operator']) && isset($this->_request['country'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $country = $this->_request['country'];
            $query = "SELECT Op.operator_name AS Operator, Op.country AS Country, tempinout.totaloptins AS 'Total Optins', tempinout.totaloptouts AS 'Total Optouts', COUNT(Ofail.optfail_ID) AS 'Total Optfails'
                        FROM operator Op, optfail Ofail,
                        (SELECT Op.operator_name AS operator, Op.country, tempin.totaloptins, COUNT(Oout.optout_ID) AS totaloptouts FROM operator Op, optouts Oout,
                        (SELECT Op.operator_name AS operator, Op.country AS country, COUNT(Oin.optin_ID) AS totaloptins FROM operator Op, optins Oin
                        WHERE Oin.operator=Op.operator_name AND Oin.date>'" . $startdate . "' AND Oin.date<'" . $enddate . "' AND Op.country='" . $country . "' GROUP BY Op.operator_name) AS tempin
                        WHERE Op.operator_name=tempin.operator AND Op.operator_name=Oout.operator AND Oout.date>'" . $startdate . "' AND Oout.date<'" . $enddate . "' GROUP BY Op.operator_name) AS tempinout
                        WHERE Op.operator_name=tempinout.operator AND Op.operator_name=Ofail.operator AND Ofail.date>'" . $startdate . "' AND Ofail.date<'" . $enddate . "' GROUP BY Op.operator_name";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } elseif (!isset($this->_request['start_date']) && !isset($this->_request['end_date']) && !isset($this->_request['operator']) && isset($this->_request['country'])) {
            $startdate = $this->_request['start_date'];
            $enddate = $this->_request['end_date'];
            $country = $this->_request['country'];
            $query = "SELECT Op.operator_name AS Operator, Op.country AS Country, tempinout.totaloptins AS 'Total Optins', tempinout.totaloptouts AS 'Total Optouts', COUNT(Ofail.optfail_ID) AS 'Total Optfails'
                        FROM operator Op, optfail Ofail,
                        (SELECT Op.operator_name AS operator, Op.country, tempin.totaloptins, COUNT(Oout.optout_ID) AS totaloptouts FROM operator Op, optouts Oout,
                        (SELECT Op.operator_name AS operator, Op.country AS country, COUNT(Oin.optin_ID) AS totaloptins FROM operator Op, optins Oin
                        WHERE Oin.operator=Op.operator_name AND Op.country='" . $country . "' GROUP BY Op.operator_name) AS tempin
                        WHERE Op.operator_name=tempin.operator AND Op.operator_name=Oout.operator GROUP BY Op.operator_name) AS tempinout
                        WHERE Op.operator_name=tempinout.operator AND Op.operator_name=Ofail.operator GROUP BY Op.operator_name";
            try {
                $select_query = $this->db->prepare($query);
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            try {
                $select_query->execute();
            } catch (PDOException $e) {

                $error = 'Error In server';
                $this->db = NULL;
                $this->response($this->json($error), 404);
                exit();
            }

            $count = $select_query->rowCount();

            if ($count == 0) {

                $this->db = NULL;
                $this->response('No Data', 200);
            } else {
                $results = $select_query->fetchAll(PDO::FETCH_ASSOC);
                $this->db = NULL;
                $this->response($this->json($results), 200);
            }
        } else {
            $error = array(
                'status' => "Failed",
                "msg" => "Please Enter Valid Parameters!"
            );
            $this->response($this->json($error), 400);
        }
    }
}


?>